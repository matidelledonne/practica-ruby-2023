# PRACTICA RUBY 2023

## PRACTICA 01-13

Dado un string, comprobar si es palíndromo. Por ejemplo:

```rb
  inputString = "aabaa", -> palindrome? (inputString) = true
  inputString = "abac", -> palindrome? (inputString) = false
  inputString = "a", -> palindrome? (inputString) = true
```

---

Dado un arreglo de strings, retornar otro el cual contenga TODOS los strings mas largos. Por ejmplo:

```rb
  inputArray = ["aba", "aa", "ad", "vcd", "aba"], se espera la salida all_longest_strings(inputArray) = ["aba", "vcd", "aba"]
```

---

Crear dos funciones para validadar un `email` usando expresiones regulares y otra sin expresiones regulares.

- El string deber contener `@`.
- El string deber contener al menos un `.` .
- El `@` no puede estar despues que el `.` . Ej: `@egmail.com` es invalido

```rb
  validate_email("pepito@gmail.com")
```
---

Crea una función que convierta un número de teléfono con letras en uno sólo con números.


| Number | Letter |
| ------ | ------ |	
| 0      |  none  |
| 1      |  none  |
| 2      |   ABC  |
| 3      |   DEF  |
| 4      |   GHI  |
| 5      |   JKL  |
| 6      |   MNO  |
| 7      |   PQRS |
| 8      |   TUV  |
| 9      |   WXYZ |

```rb
  text_to_num("123-647-EYES") ➞ "123-647-3937"

  text_to_num("(325)444-TEST") ➞ "(325)444-8378"

  text_to_num("653-TRY-THIS") ➞ "653-879-8447"

  text_to_num("435-224-7613") ➞ "435-224-7613"
```

Ayudita: Revisar `Hash` y metodo `gsub`

---

Sea el siguiente dump de base de datos: `people.csv`

|id|name|email|birth_date|created_at|updated_at|
| -- | -- | -- | -- | -- | -- |
|1|Burks, Rosella|burks@example.com|2006-07-15|2016-07-15 18:16:13|2016-07-15 18:16:13|
|2|Avila, Damien|avila@example.com|2001-12-15|2016-07-15 18:16:13|2016-07-15 18:16:13|
|3|Olsen, Robin|olsen@example.com|1998-04-25|2016-07-15 18:16:13|2016-07-15 18:16:13|
|4|Moises, Edgar|moises@example.com|2004-02-28|2016-07-15 18:16:13|2016-07-15 18:16:13|
|5|Claude, Elvin|claude@example.com|1996-07-31|2016-07-15 18:16:13|2016-07-15 18:16:13|

- Imprimir los mails de las personas mayores a 18 años.
- Ordenar por fecha de cumpleaños.


## PRACTICA 01-16

Utilizar el archivo `main.rb`.
Hacer un wrapper para el JSON `swapi.json` modelando las identidades:

- Person
- Starship
- Vehicle

Crear una clase padre para manejar la conexión con la DB.

Crear Modulos para manejar las relaciones o re utilizar metodos.

### Interfaz esperada

```rb
person_data = Person.new('people')
puts person_data.find_by_key_value('name', 'Luke Skywalker')
=> {"id"=>1, "name"=>"Luke Skywalker", "height"=>"172", "mass"=>"77", "hair_color"=>"blond", "skin_color"=>"fair", "eye_color"=>"blue", "birth_year"=>"19BBY", "gender"=>"male", "vehicles"=>[14, 30], "starships"=>[12, 22]}
puts person_data.find_by_name('Obi-Wan Kenobi')
=> {"id"=>10, "name"=>"Obi-Wan Kenobi", "height"=>"182", "mass"=>"77", "hair_color"=>"auburn, white", "skin_color"=>"fair", "eye_color"=>"blue-gray", "birth_year"=>"57BBY", "gender"=>"male", "vehicles"=>[38], "starships"=>[48, 59, 64, 65, 74]}

# Replicar para los demas

```

---

Crear una Gema que contenga las Clases y Modulos de la Práctica anterior.

Crear una API con [Sinatra](http://sinatrarb.com/):

- Debe tener a endpoint /people/:id, me devuelva un personaje con todo la info relacionada.
- Debe ser un projecto con su propio Gemfile
- Debe tener como dependencia unicamente Sinatra y nuestra gema wrapper
